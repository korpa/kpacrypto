package kpacrypto

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha512"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"strings"
)

//CreateRandom returns a random string with length n
func CreateRandom(n int) (string, error) {

	//Why we use crypto/rand instead of math/rand
	//https://twitter.com/FiloSottile/status/1164408930761658368

	bytes := make([]byte, n)
	_, err := rand.Read(bytes)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return "", err
	}

	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	for i, b := range bytes {
		bytes[i] = letters[b%byte(len(letters))]
	}
	return string(bytes), nil

}

//HashAES creates a HMAC-SHA512 hash and returns 32 bytes of it
func HashAES(salt []byte, key []byte) [32]byte {
	hm := hmac.New(sha512.New512_256, salt)
	hm.Write(key)

	var key32 [32]byte
	copy(key32[:], hm.Sum(nil))

	return key32
}

//EncryptString encrypts a string
func EncryptString(salt string, key string, content string) (string, error) {
	keyHash := HashAES([]byte(salt), []byte(key))

	ciphertext, _ := encrypt([]byte(content), &keyHash)
	encodedContent := base64.StdEncoding.EncodeToString([]byte(ciphertext))

	return fmt.Sprintf("ssckpl_vault:v1.0:AES-256-GCM::%s", encodedContent), nil
}

func DecryptString(salt string, key string, markAndEncodedContent string) (string, error) {

	s := strings.Split(markAndEncodedContent, "::")
	mark, encodedContent := s[0], s[1]

	if mark != "ssckpl_vault:v1.0:AES-256-GCM" {
		return "", errors.New("unknown encryption type: '" + mark + "'")
	}

	keyHash := HashAES([]byte(salt), []byte(key))

	decoded, err := base64.StdEncoding.DecodeString(encodedContent)
	if err != nil {
		return "", err
	}
	decryptedContent, err := decrypt(decoded, &keyHash)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s", decryptedContent), nil
}

// encrypt encrypts data using 256-bit AES-GCM
func encrypt(plaintext []byte, key *[32]byte) (ciphertext []byte, err error) {
	block, err := aes.NewCipher(key[:])
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	_, err = io.ReadFull(rand.Reader, nonce)
	if err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

// decrypt decrypts data using 256-bit AES-GCM
func decrypt(ciphertext []byte, key *[32]byte) (plaintext []byte, err error) {
	block, err := aes.NewCipher(key[:])
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	if len(ciphertext) < gcm.NonceSize() {
		return nil, errors.New("malformed ciphertext")
	}

	return gcm.Open(nil,
		ciphertext[:gcm.NonceSize()],
		ciphertext[gcm.NonceSize():],
		nil,
	)
}
