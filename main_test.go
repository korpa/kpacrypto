package kpacrypto

import (
	"bytes"
	"testing"
)

func TestEncryptDecryptGCM(t *testing.T) {

	salt, err := CreateRandom(20)
	if err != nil {
		t.Error(err)
	}
	key, err := CreateRandom(20)
	if err != nil {
		t.Error(err)
	}

	gcmTests := []struct {
		plaintext string
		salt      string
		key       string
	}{
		{
			plaintext: "Hello, world!",
			key:       key,
			salt:      salt,
		},
	}

	for _, tt := range gcmTests {
		ciphertext, err := EncryptString(tt.salt, tt.key, tt.plaintext)
		if err != nil {
			t.Fatal(err)
		}

		plaintext, err := DecryptString(tt.salt, tt.key, ciphertext)
		if err != nil {
			t.Fatal(err)
		}

		if !bytes.Equal([]byte(plaintext), []byte(tt.plaintext)) {
			t.Errorf("plaintexts don't match")
		}

		ciphertextBytes := []byte(ciphertext)
		ciphertextBytes[0] ^= 0xff
		ciphertext = string(ciphertextBytes)
		plaintext, err = DecryptString(tt.salt, tt.key, ciphertext)
		if err == nil {
			t.Errorf("gcmOpen should not have worked, but did")
		}
	}
}
